/*
 * FYQ5641.c
 */
#include "FYQ5641.h"

uint16_t currentDig = DIG1_PIN;
uint16_t value = 0;
uint16_t values[4] = {0,0,0,0};

extern TIM_HandleTypeDef htim2;


void FYQ5641_init(void) {
	__GPIOD_CLK_ENABLE();
	__GPIOE_CLK_ENABLE();


	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Pin = ALL_SEGMENTS;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SEGMENTS_PORT, &GPIO_InitStructure);
	HAL_GPIO_WritePin(SEGMENTS_PORT, ALL_SEGMENTS, SEGMENT_RESET);

	GPIO_InitStructure.Pin = DIG1_PIN | DIG2_PIN | DIG3_PIN | DIG4_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(DIG_PORT, &GPIO_InitStructure);
	HAL_GPIO_WritePin(DIG_PORT, ALL_DIGS, DIG_SET);

}

void  FYQ5641_off(uint16_t dig) {
	HAL_GPIO_WritePin(DIG_PORT, ALL_DIGS, DIG_SET);
	HAL_GPIO_WritePin(SEGMENTS_PORT, ALL_SEGMENTS, SEGMENT_RESET);
}
void FYQ5641_init_DIG(uint16_t dig) {
	HAL_GPIO_WritePin(DIG_PORT, ALL_DIGS, DIG_RESET);
	HAL_GPIO_WritePin(DIG_PORT, dig, DIG_SET);
	HAL_GPIO_WritePin(SEGMENTS_PORT, ALL_SEGMENTS, SEGMENT_RESET);
}

void FYQ5641_on (uint16_t dig, uint8_t value) {
	FYQ5641_init_DIG(dig);

	uint16_t pins = NUMBER0;
	switch(value) {
		case 0:
			pins = NUMBER0;
			break;
		case 1:
			pins = NUMBER1;
			break;
		case 2:
			pins = NUMBER2;
			break;
		case 3:
			pins = NUMBER3;
			break;
		case 4:
			pins = NUMBER4;
			break;
		case 5:
			pins = NUMBER5;
			break;
		case 6:
			pins = NUMBER6;
			break;
		case 7:
			pins = NUMBER7;
			break;
		case 8:
			pins = NUMBER8;
			break;
		case 9:
			pins = NUMBER9;
			break;
		default:
			pins = ERROR;
	}
	HAL_GPIO_WritePin(SEGMENTS_PORT, pins, SEGMENT_SET);
}

void FYQ5641_next(void) {
	switch (currentDig) {
		case DIG1_PIN:
			if (values[0] > 0) {
				FYQ5641_on(currentDig, values[0]);
			}
			currentDig = DIG2_PIN;
			break;
		case DIG2_PIN:
			if (values[0] > 0 || values[1] > 0) {
				FYQ5641_on(currentDig, values[1]);
			}
			currentDig = DIG3_PIN;
			break;
		case DIG3_PIN:
			if (values[0] > 0 || values[1] > 0 || values[2] > 0) {
				FYQ5641_on(currentDig, values[2]);
			}
			currentDig = DIG4_PIN;
			break;
		case DIG4_PIN:
			FYQ5641_on(currentDig, values[3]);
			currentDig = DIG1_PIN;
			break;
	}
}

void FYQ5641_value(uint16_t newValue) {
	value = newValue;
	values[0] = (value%10000)/1000;
	values[1] = (value%1000)/100;
	values[2] = (value%100)/10;
	values[3] = (value%10);
}

