/*
 * GPIO.c
 *
 *  Created on: 24 апр. 2016 г.
 *      Author: pervoliner
 */
#include "GPIO.h"
#include "diag/Trace.h"

void _sensor(void) {
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Pin = SENSOR_PIN;
	GPIO_InitStructure.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SENSOR_PORT, &GPIO_InitStructure);

	HAL_NVIC_SetPriority(SENSOR_IRQ, 0, 0);
	HAL_NVIC_EnableIRQ(SENSOR_IRQ);
}

void _indicator() {
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Pin = GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStructure);

}

void _indicator_toggle(void) {
		HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
		HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_13);
		HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_14);
		HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);

}

void GPIO_init(void) {
	__GPIOA_CLK_ENABLE();
	__GPIOB_CLK_ENABLE();
	__GPIOD_CLK_ENABLE();

	_sensor();
	_indicator();
}

//void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
//	if (GPIO_Pin == SENSOR_PIN) {
//		//trace_printf("SENSOR_PIN\n");
//		 //trace_printf("System clock: %u Hz\n", SystemCoreClock);
//	}
//}
