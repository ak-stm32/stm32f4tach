/*
 * Timer2.h
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */

#ifndef TIMER2_H_
#define TIMER2_H_

#include "stm32f4xx_hal.h"

void Timer2_init(void);

void Timer2_init_IRQ(void);

#endif /* TIMER2_H_ */
