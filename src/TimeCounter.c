/*
 * TimeCounter.c
 *
 *  Created on: 26 May 2016
 *      Author: akoiro
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "diag/Trace.h"

uint16_t counterCurrent = 0;
uint16_t counterNew = 0;
uint16_t valueSec = 0;

uint16_t TimerCounter_value(void) {
	return counterCurrent == 0 ? counterCurrent : round(60000.0/counterCurrent);
}


void TimerCounter_increment(void) {
	if (counterNew%1000 == 0) {
		valueSec++;
	}
	counterNew++;
}

void TimerCounter_reset(void) {
	counterCurrent = counterCurrent > 0 ? (counterCurrent + counterNew)/2.0 : counterNew;
	counterNew = 0;
}


