/*
 * Timer4.c
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */
#include "Timer4.h"

TIM_HandleTypeDef htim4;

/**
 * 1sec
 * htim4.Init.Prescaler = 8000;
 * htim4.Init.Period = 1000;
 *
 *
 */
void Timer4_init(void) {
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	  __TIM4_CLK_ENABLE();
	  htim4.Instance = TIM4;
	  //1Mhzx2->2MHz/50000 = 40Hz/40
	  //1MHzx2->2MHz/5000 = 400Hz
	  //10MHz/25000 = 400Hz
	  htim4.Init.Prescaler = 1000;
	  htim4.Init.Period = 60;
	  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
	  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	  htim4.Init.RepetitionCounter = 0;
	  HAL_TIM_Base_Init(&htim4);

	  HAL_TIM_Base_Start_IT(&htim4);

	  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	   HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig);

	   sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	   sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	   HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig);
}

void Timer4_init_IRQ(void) {
	  HAL_NVIC_SetPriority(TIM4_IRQn, 0, 0);
	  HAL_NVIC_EnableIRQ(TIM4_IRQn);
}
