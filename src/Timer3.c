/*
 * Timer3.c
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */
#include "Timer3.h"

TIM_HandleTypeDef htim3;

//1/3 = x/1000 3x=1000 x=1000/3 333

/**
 * We will calculate number of 1/1000 sec beetween two pulse
 *
 *  for example:
 *  value = 1000 -> 1 pulse/sec -> 60 pulses/min
 *  value = 1 -> 1000 pulse/sec -> 60000 pulse/min
 *  value = 2 -> 500 pulse/sec -> 30000 pulse/min
 *  value = 3 -> 333 pulse/sec -> 19980 pulse/min
 *  value = 4 -> 250 pulse/sec -> 15000 pulse/min
 *  vaule = 5 -> 200 pulse/sec -> 12000 pulse/min
 *  value = 6 -> 166.6666 pulse/sec -> 10000 pulse/min
 *
 * 8MHz
 * 1 sec
 * Init.Prescaler = 8000;
 * Init.Period = 1000;
 *
 * 1/10 sec
 * Init.Prescaler = 8000;
 * Init.Period = 100;
 *
 * 1/100 sec
 * Init.Prescaler = 8000;
 * Init.Period = 10;
 *
 * 1/1000 sec
 * Init.Prescaler = 100;
 * Init.Period = 80;
 *
 * !!! we should use bigger Period to get precision
 */
void Timer3_init(void) {
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	  __TIM3_CLK_ENABLE();
	  htim3.Instance = TIM3;
	  //1Mhzx2->2MHz/50000 = 40Hz/40 = 1Hz
	  //1MHzx2->2MHz/5000 = 400Hz
	  //10MHz/50000 = 200Hz
	  htim3.Init.Prescaler = 100;
	  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	  htim3.Init.Period = 79;
	  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	  htim3.Init.RepetitionCounter = 0;
	  HAL_TIM_Base_Init(&htim3);

	  HAL_TIM_Base_Start_IT(&htim3);

	  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	   HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig);

	   sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	   sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	   HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig);
}

void Timer3_init_IRQ(void) {
	  HAL_NVIC_SetPriority(TIM3_IRQn, 0, 0);
	  HAL_NVIC_EnableIRQ(TIM3_IRQn);
}

//Vk = (PI * Dk * nk)/1000*60
//nk = Vk*1000*60/PI*Dk
