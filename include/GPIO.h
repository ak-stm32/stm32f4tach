/*
 * GPIO.h
 *
 *  Created on: 24 ���. 2016 �.
 *      Author: pervoliner
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"

#define SENSOR_PIN		GPIO_PIN_1
#define SENSOR_PORT		GPIOA
#define SENSOR_IRQ 		EXTI1_IRQn

extern void GPIO_init(void);

#endif /* GPIO_H_ */
