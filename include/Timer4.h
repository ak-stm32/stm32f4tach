/*
 * Timer4.h
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */

#ifndef TIMER4_H_
#define TIMER4_H_

#include "stm32f4xx_hal.h"

void Timer4_init(void);

void Timer4_init_IRQ(void);

void Timer4_PeriodElapsedCallback(void);

#endif /* TIMER4_H_ */
