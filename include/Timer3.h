/*
 * Timer3.h
 *
 *  Created on: 1 ��� 2016 �.
 *      Author: pervoliner
 */

#ifndef TIMER3_H_
#define TIMER3_H_

#include "stm32f4xx_hal.h"

void Timer3_init(void);

void Timer3_init_IRQ(void);

void Timer3_PeriodElapsedCallback(void);

#endif /* TIMER3_H_ */
