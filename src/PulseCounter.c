/*
 * PulseCounter.c
 *
 *  Created on: 26 May 2016
 *      Author: akoiro
 */

#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"

/**
 * to count pulses
 */
uint16_t count[100];
uint16_t index = 0;


void PulseCounter_inti(void) {
	memset(count, 0, 100 * sizeof(count[0]));
}

uint16_t sum(uint16_t number) {
	uint16_t i, total = 0;
	for (i =0; i < number; i++ ) {
		total += count[i];
	}
	return total;
}

void recalc(uint16_t number) {
	  if (index == (number - 1) ) {
		  index = 0;
		  count[index] = 0;
	  } else {
		  index++;
		  count[index] = 0;
	  }
}

void reset(void) {
	count[0] = 0;
	index = 0;
}

void increment(void) {
	count[index]++;
}
